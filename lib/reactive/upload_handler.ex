defmodule Reactive.UploadHandler do
  require Logger

  def init(type, req, settings) do
    {:ok, req, %{ settings: settings }}
  end

  def handle(req, state) do
    {method,req} = :cowboy_req.method(req)
   #Logger.debug("HANDLER #{inspect method} #{inspect req}")
    case method do
      "POST" ->
        reqU = case multipart(req,state) do
          {:ok, reqM} ->
            {:ok,req3} = :cowboy_req.reply(200, Map.get(state.settings,:headers,[]), "ok", reqM)
            req3
          {:rejected, reason, reqM} ->
            {:ok,req3} = :cowboy_req.reply(500, [], reason, reqM)
            req3
        end
        {:ok, reqU, state}
      _ ->
        {:ok, req3} = :cowboy_req.reply(503, [], "<h1>Multipart upload only</h1>", req)
        {:ok, req3, state}
    end
  end

  def multipart(req,state) do
    max_upload_size = Map.get(state.settings,:max_upload_size,327200)
    prefix = Map.get(state.settings,:prefix,"uploads/")
    prefix_len = :erlang.byte_size(prefix)

    {<< prefix :: binary-size(prefix_len) , path_id :: binary >>,_} = :cowboy_req.path(req)
    id = String.replace(String.replace(path_id,"..",""),"/","")

    upload = [Reactive.Upload,id]

    upload_path = Map.get(state.settings,:upload_dir,"uploads/")<>id<>"-"

    {_,upload_size,req} = :cowboy_req.parse_header("content_length",req,0)


#    Logger.debug("UP Limit #{inspect upload_size} #{inspect max_upload_size}")
    if(upload_size>max_upload_size) do
      Reactive.Entity.event(upload,:upload_rejected)
      Logger.debug("SIZE LIMIT #{inspect upload_size} #{inspect max_upload_size}")
      {:rejected,"size_limit",req}
    else
      part(req,state,upload,upload_path,0,upload_size,max_upload_size)
    end
  end

  def part(req,state,upload,upload_path,uploaded,size,max_size) do
    #Logger.debug("GET PART!")
    case :cowboy_req.part(req) do
      {:ok, headers, req2} ->
     #   Logger.debug("GOT PART!")
        {req5,nuploaded} = case :cow_multipart.form_data(headers) do
          {data, fieldName} ->
            {:ok, body, req3} = :cowboy_req.part_body(req2)
            Reactive.Entity.event(upload,{:field,fieldName,body})
            {req3,:erlang.byte_size(body)+uploaded}
          {file, fieldName, filename, _CType, _CTransferEncoding} ->
            file_name = :filename.absname(upload_path <> Regex.replace(~r/[^a-zA-Z0-9._-]/,fieldName<>"-"<>filename,"_"))
            Reactive.Entity.event(upload,{:file,fieldName,file_name,filename})
            Logger.info("UPloading to #{file_name}")
            {:ok, ioDevice} = :file.open(file_name, [:raw, :write])
            sres = stream_file(req2, ioDevice, upload, uploaded, size, max_size)
            :ok = :file.close(ioDevice)
            case sres do
              {:ok, nup, req4} ->
                {req4,nup}
              {:limit, req4} ->
                {10000000000000,req4} # elephant in cairo
            end
        end
        if(nuploaded>max_size) do
          Reactive.Entity.event(upload,:upload_rejected)
          Logger.debug("SIZE LIMIT #{inspect nuploaded} #{inspect max_size}")
          {:rejected,"size_limit",req}
        else
          Reactive.Entity.event(upload,{:upload_progress,nuploaded,size})
          part(req5,state,upload,upload_path,nuploaded,size,max_size)
        end
      {done, req2} ->
        Reactive.Entity.event(upload,:upload_finished)
        {:ok, req2}
    end
  end

  def stream_file(req, ioDevice, upload, uploaded, size, max_size) do
    {control, data, req2} = :cowboy_req.part_body(req)
    nuploaded = byte_size(data) + uploaded
    if(nuploaded > max_size) do
      {:limit, req2}
    else
      :ok = :file.write(ioDevice, data)
      Reactive.Entity.event(upload,{:upload_progress,nuploaded,size})
      case control do
        :ok -> {:ok, nuploaded, req2}
        :more -> stream_file(req2, ioDevice, upload, uploaded, size, max_size)
      end
    end
  end

  def terminate(reason, req, state) do
    :ok
  end
end