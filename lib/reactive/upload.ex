defmodule Reactive.Upload do
  use Reactive.TemporaryEntity

  def init([id]) do
    {:ok,%{
      id: id,
      size: :undefined,
      uploaded: :undefined,
      state: :not_started,
      last_progress_notify: 0,
      fields: []
    },%{}}
  end

  def get(:progress,state) do
    {:reply, %{size: state.size, uploaded: state.uploaded, state: state.state}, state}
  end

  def get(:state,state) do
    {:reply, state.state, state}
  end

  def event({:upload_progress,uploaded,size},state,_from) do
    if(Reactive.Entity.timestamp()>state.last_progress_notify+300) do
      notify_observers(:progress,{:set,[%{ size: size, uploaded: uploaded, finished: false }]})
      if(state == :not_started) do
        notify_observers(:state,{:set,[:uploading]})
      end
      %{ state | size: size, uploaded: uploaded, last_progress_notify: Reactive.Entity.timestamp(), state: :uploading  }
    else
      %{ state | size: size, uploaded: uploaded, state: :uploading }
    end
  end

  def event({:field,name,value},state,_from) do
    fd=%{ field: name, value: value}
    nfields=[fd | state.fields]
    notify_observers(:fields,{:push,[fd]})
    %{state | fields: nfields}
  end

  def event({:file,field_name,file_name,src_file_name},state,_from) do
    fd=%{ field: field_name, file: file_name, src_file_name: src_file_name }
    nfields=[fd | state.fields]
    notify_observers(:fields,{:push,[fd]})
    %{state | fields: nfields}
  end

  def event(:upload_finished,state,_from) do
    notify_observers(:progress,{:set,[%{ size: state.size, uploaded: state.uploaded, finished: true }]})
    notify_observers(:state,{:set,[:finished]})
    %{ state |  state: :finished }
  end

  def event(:upload_rejected,state,_from) do
    delete_files(state.fields)
    notify_observers(:progress,{:set,[%{ size: state.size, uploaded: state.uploaded, finished: true }]})
    notify_observers(:state,{:set,[:rejected]})
    %{ state | state: :rejected }
  end

  def request({:process,processor,id},state,_from,_rid) do
    result=apply(processor,:process_upload,[state.fields,id])
    delete_files(state.fields)
    {:reply,result,state}
  end

  def delete_files(fields) do
    Enum.each(fields,fn
      (%{ file: file }) -> :file.delete(file)
      (_) -> :ok
    end)
  end
end
